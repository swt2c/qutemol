#ifndef __VCGLIB_EDGE_AE_TYPE
#define __VCGLIB_EDGE_AE_TYPE

#define EDGE_TYPE EdgeAE 

#define __VCGLIB_EDGE_AE

#include <vcg/simplex/edge/base.h> 

#undef EDGE_TYPE 

#undef __VCGLIB_EDGE_AE

#endif
