#ifndef __VCGLIB_TETRA_ATTN_TYPE
#define __VCGLIB_TETRA_ATTN_TYPE

#define TETRA_TYPE TetraATTN

#define __VCGLIB_TETRA_AT
#define __VCGLIB_TETRA_TN
#include <vcg/simplex/tetrahedron/base.h> 

#undef TETRA_TYPE 

#undef __VCGLIB_TETRA_AT
#undef __VCGLIB_TETRA_TN

#endif