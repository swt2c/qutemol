#ifndef __VCGLIB_FACE_AFFC_TYPE
#define __VCGLIB_FACE_AFFC_TYPE

#define FACE_TYPE FaceAFFC 

#define __VCGLIB_FACE_AF
#define __VCGLIB_FACE_FC

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_AF
#undef __VCGLIB_FACE_FC

#endif
